## Welcome
This is a seed project with following features:

* GitLab CI
    * Secrets stored in protected environment variables
    * Can be run with GitLab's runners or autoscaled on AWS or other clouds.  (https://docs.gitlab.com/runner/configuration/autoscale.html)
    * Configured to automatically run code quality on branches
    * Deploys to multiple environments
    * Alpha (develop) and Staging (master) deployed automatically after merges
    * Production deploy is manually triggered after Staging inspection
* Monorepo
    * Shared code
    * Clear boundaries
* Docker
    * GitLab docker repository
    * Custom job runner image for quicker spin up
* Firebase
    * Hosting: Client
    * API: Firebase functions
* Client
    * Vue w/ Vuetify
    * Written in Typescript
    * Vue-Class-Component and Vue-Property decorator for nicer coding experience
    * ITCSS structure for styles
    * Storybook for developing and testing components without running the application
    * Unit testing with Jest
    * Snapshot testing with Jest and Storybook
    * Linting with ESLint
    * You could also add Cypress testing
* Server
    * Firebase functions simplify development, deployment, and scaling of API
    * Unit testing with Jest
    * Linting with ESLint

### Starting
Requires node version 10 to be installed (recommended to use nvm)

```
$ npm install -g firebase-tools
$ yarn
$ yarn start
```

```
client: localhost:3000
endpoint: http://localhost:5001/fir-base-vue-alpha-8d565/us-central1/playRockPaperScissors
```

Interact with endpoint without client:
```
curl -d '{"data": "Rock"}' -H "Content-Type: application/json" -X POST http://localhost:5001/fir-base-vue-alpha-8d565/us-central1/playRockPaperScissors
```

#### Run Storybook for the client
```
yarn storybook
```

#### Run unit tests
```
yarn test:unit
```

#### Lints and fixes files
```
yarn lint
```
