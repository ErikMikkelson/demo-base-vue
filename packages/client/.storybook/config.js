import "@/styles/main.scss"
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport'
import { configure, addParameters, addDecorator } from '@storybook/vue'
import vuetify from "@/plugins/vuetify"

// Vuetify has a 12 point grid system. Built using flex-box, the grid is used to layout an application's content. It
// contains 5 types of media breakpoints that are used for targeting specific screen sizes and orientations.
const vuetifyViewports = {
  VuetifyLg: {
    name: 'Vuetify LG',
    styles: {
      width: '1904px',
      height: "100%"
    },
    type: 'desktop'
  },
  VuetifyXs: {
    name: 'Vuetify XS',
    styles: {
      width: '600px',
      height: "100%"
    },
    type: 'mobile'
  },
  VuetifySm: {
    name: 'Vuetify SM',
    styles: {
      width: '960px',
      height: "100%"
    },
    type: 'mobile'
  },
  VuetifyMd: {
    name: 'Vuetify MD',
    styles: {
      width: '1264px',
      height: "100%"
    },
    type: 'tablet'
  },
  VuetifyXl: {
    name: 'Vuetify XL',
    styles: {
      width: '4096px',
      height: "100%"
    },
    type: 'desktop'
  }
};

addParameters({
  viewport: {
    defaultViewport: 'VuetifyMd',
    viewports: {
      ...vuetifyViewports,
      ...INITIAL_VIEWPORTS
    }
  }
});

addDecorator(() => ({
  vuetify,
  template: '<v-app><v-content><v-container><v-layout column><story/></v-layout></v-container></v-content></v-app>',
}));

const req = require.context("../src", true, /\.stories\.ts$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
