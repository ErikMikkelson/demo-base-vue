const path = require('path');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

module.exports = async ({ config }) => {
    config.module.rules = config.module.rules.filter(rule =>!rule.test.test('.scss'));

    config.resolve = {
        extensions: ['.js', '.ts', '.vue', '.json'],
        alias: {
            vue$: 'vue/dist/vue.esm.js',
            'assets': path.resolve('../src/assets'),
            '@': path.join(__dirname, '../src'),
            'components': path.join(__dirname, '../src/components'),
            'plugins': path.join(__dirname, '../src/plugins'),
            'styles': path.join(__dirname, '../src/styles'),
            'views': path.join(__dirname, '../src/views'),
        }
    };

    config.module.rules.push({
        test: /\.sass$/,
        use: [
            'vue-style-loader',
            'css-loader', {
                loader: 'sass-loader',
                options: {
                    prependData: `@import "@/styles/_globals.scss"`
                }
            },
        ],
    });

    config.module.rules.push({
        test: /\.scss$/,
        use: [
            'vue-style-loader',
            'css-loader', {
                loader: 'sass-loader',
                options: {
                    prependData: `@import "@/styles/_globals.scss";`
                }
            },
        ],
    });

    config.module.rules.push({
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [
            {
                loader: 'ts-loader',
                options: {
                    appendTsSuffixTo: [/\.vue$/],
                    transpileOnly: true
                },
            }
        ]
    });

    config.plugins.push(new ForkTsCheckerWebpackPlugin())

    return config;
};