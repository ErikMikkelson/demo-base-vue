module.exports = {
    setupFiles: ["<rootDir>/test/setup.ts"],
    moduleFileExtensions: ["js", "jsx", "json", "vue", "ts", "tsx"],
    transform: {
        "^.+\\.vue$": "vue-jest",
        ".+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$": "jest-transform-stub",
        "^.+\\.tsx?$": "ts-jest",
        "^.+\\.jsx?$": "babel-jest"
    },
    transformIgnorePatterns: ["node_modules/(?!(vuetify/|@storybook/.*\\.vue$))"],
    moduleNameMapper: {
        "^@/(.*)$": "<rootDir>/src/$1"
    },
    snapshotSerializers: ["jest-serializer-vue"],
    testMatch: ["<rootDir>/src/**/(*.)spec.(js|ts)"],
    testURL: "http://localhost/",
    watchPlugins: ["jest-watch-typeahead/filename", "jest-watch-typeahead/testname"],
    globals: {
        "ts-jest": {
            babelConfig: true
        }
    }
};
