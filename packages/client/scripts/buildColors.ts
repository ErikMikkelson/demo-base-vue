import { flow, mapValues, isString, map, kebabCase } from "lodash-es";
import fs from "fs";
import path from "path";
import Colors from "../src/styles/Colors";

const stylesPath = "./src/styles/settings";

async function buildColors(): Promise<void> {
    const maps = flow(
        colors =>
            mapValues(colors, colors => {
                if (isString(colors)) {
                    return colors;
                }

                return map(colors, (color, name) => `    ${name}: ${color}`);
            }),
        colors =>
            map(colors, (lines, name) => {
                if (isString(lines)) {
                    return `$color-${kebabCase(name)}: ${lines};\n`;
                }

                return `$color-${kebabCase(name)}: (\n${lines.join(
                    ",\n"
                )}\n);\n`;
            })
    )(Colors);

    const scss = maps.join("\n");

    const colorsStyleFile = path.join(stylesPath, "_colors.scss");
    await fs.promises.writeFile(colorsStyleFile, scss);
}

buildColors();
