// config.js
const env = process.env.NODE_ENV || "development";

// DO NOT PUT SECRETS IN THIS FILE
// Secrets should be stored in the environment either by CI or host

const defaults = {
    FIREBASE_CONFIG: {
        apiKey: "AIzaSyBHe-P9CVkgTADubOnwScgRdB0c5nlL1WE",
        authDomain: "revel-production-35dfd.firebaseapp.com",
        databaseURL: "https://revel-production-35dfd.firebaseio.com",
        projectId: "revel-production-35dfd",
        storageBucket: "revel-production-35dfd.appspot.com",
        messagingSenderId: "472768233840",
        appId: "1:472768233840:web:24ff27defaf542dd01cfb5",
        measurementId: "G-JMX7ZSHF25"
    }
};

const development = {
    ...defaults,
    ENVIRONMENT: "development",
    DB_SETTINGS: {
        host: "localhost:8080",
        ssl: false
    },
    EMULATOR_URL: "http://localhost:5001"
};

const test = {
    ...defaults,
    ENVIRONMENT: "test"
};

const alpha = {
    ...defaults,
    ENVIRONMENT: "alpha"
};

const production = {
    ...defaults,
    ENVIRONMENT: "production"
};

const config: any = {
    development,
    test,
    alpha,
    production
};

export default config[env];
