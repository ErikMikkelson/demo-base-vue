import Colors, { ColorsClass } from "@/styles/Colors";
import Vue from "vue";

declare module "vue/types/vue" {
    interface Vue {
        $colors: ColorsClass;
    }
}

const colors = {
    install(_Vue: any) {
        Object.defineProperty(_Vue.prototype, "$colors", {
            get() {
                return Colors;
            }
        });
    }
};

Vue.use(colors);
