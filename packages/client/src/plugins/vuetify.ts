import Vue from "vue";
import Vuetify, {
    VApp,
    VBtn,
    VCard,
    VCardText,
    VContainer,
    VContent,
    VFlex,
    VHover,
    VIcon,
    VImg,
    VLayout,
    VTooltip
} from "vuetify/lib";
import Colors from "@/styles/Colors";

Vue.use(Vuetify, {
    components: {
        // Vuetify components used in stories need to be manually registered.
        // vuetify-loader cannot pick them up automatically.
        VApp,
        VBtn,
        VCard,
        VCardText,
        VContainer,
        VContent,
        VFlex,
        VHover,
        VIcon,
        VImg,
        VLayout,
        VTooltip
    }
});

export default new Vuetify({
    theme: {
        dark: false,
        options: {
            customProperties: true
        },
        themes: {
            light: {
                primary: Colors.primary[500],
                secondary: Colors.secondary[500],
                accent: Colors.accent[500],
                error: Colors.error,
                info: "#2196F3",
                success: Colors.success,
                warning: Colors.warn
            }
        }
    }
});
