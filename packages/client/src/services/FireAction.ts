import { functions } from "./Firebase";

export async function fireAction<T>(name: string, payload?: object): Promise<T> {
    const httpCall = functions.httpsCallable(name);
    const result = await httpCall(payload);

    return result.data;
}
