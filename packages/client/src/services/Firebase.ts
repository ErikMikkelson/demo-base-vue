import * as firebase from "firebase/app";

import "firebase/firestore";
import "firebase/functions";

// process.env.VUE_APP_* variables are replaced at COMPILE TIME with environment appropriate settings.
// See https://cli.vuejs.org/guide/mode-and-env.html#modes

const firebaseConfig = {
    apiKey: process.env.VUE_APP_FIREBASE_API_KEY,
    authDomain: process.env.VUE_APP_FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.VUE_APP_FIREBASE_DATABASE_URL,
    projectId: process.env.VUE_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.VUE_APP_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.VUE_APP_FIREBASE_APP_ID,
    measurementId: process.env.VUE_APP_FIREBASE_MEASUREMENT_ID
};

firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore();

if (process.env.VUE_APP_STAGE === "development") {
    db.settings({
        host: "localhost:8080",
        ssl: false
    });

    firebase.functions().useFunctionsEmulator("http://localhost:5001");
}

export const functions = firebase.functions();
