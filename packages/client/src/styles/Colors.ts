import tinyColor from "tinycolor2";

const primaryColor = "#f58a56";
const secondaryColor = "#7c5e90";
const accentColor = "#e36f78";

function lighten(baseColor: string, amount: number): string {
    return tinyColor(baseColor)
        .lighten(amount)
        .toHexString();
}

function darken(baseColor: string, amount: number): string {
    return tinyColor(baseColor)
        .darken(amount)
        .toHexString();
}

export class ColorsClass {
    primary = {
        50: lighten(primaryColor, 25),
        100: lighten(primaryColor, 20),
        200: lighten(primaryColor, 15),
        300: lighten(primaryColor, 10),
        400: lighten(primaryColor, 5),
        500: primaryColor,
        600: darken(primaryColor, 5),
        700: darken(primaryColor, 10),
        800: darken(primaryColor, 15),
        900: darken(primaryColor, 20),
        950: darken(primaryColor, 25)
    };

    secondary = {
        50: lighten(secondaryColor, 25),
        100: lighten(secondaryColor, 20),
        200: lighten(secondaryColor, 15),
        300: lighten(secondaryColor, 10),
        400: lighten(secondaryColor, 5),
        500: secondaryColor,
        600: darken(secondaryColor, 5),
        700: darken(secondaryColor, 10),
        800: darken(secondaryColor, 15),
        900: darken(secondaryColor, 20),
        950: darken(secondaryColor, 25)
    };

    accent = {
        50: lighten(accentColor, 25),
        100: lighten(accentColor, 20),
        200: lighten(accentColor, 15),
        300: lighten(accentColor, 10),
        400: lighten(accentColor, 5),
        500: accentColor,
        600: darken(accentColor, 5),
        700: darken(accentColor, 10),
        800: darken(accentColor, 15),
        900: darken(accentColor, 20),
        950: darken(accentColor, 25)
    };

    layout = {
        50: "#F5F6F8",
        100: "#EBEEF1",
        200: "#E1E5E9",
        300: "#CBD2D9",
        400: "#A6B2BE",
        500: "#8394A5",
        600: "#5E7183",
        700: "#495866",
        800: "#333D47",
        900: "#252D34",
        950: "#1c1b2c"
    };

    textLight = {
        primary: this.layout[50],
        secondary: this.layout[300],
        muted: this.layout[400],
        disabled: this.layout[600]
    };

    textDark = {
        primary: this.layout[900],
        secondary: this.layout[700],
        muted: this.layout[600],
        disabled: this.layout[500]
    };

    success = primaryColor;
    error = "#B1334C";
    warn = "#B2BC4C";
}

export default new ColorsClass();
