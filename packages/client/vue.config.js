module.exports = {
    transpileDependencies: ["vuetify"],
    pluginOptions: {
        storybook: {
            allowedPlugins: ["VuetifyLoaderPlugin"]
        }
    },
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "@/styles/_globals.scss"`
            },
            scss: {
                prependData: `@import "@/styles/_globals.scss";`
            }
        }
    },
    devServer: {
        port: 3000
    }
};
