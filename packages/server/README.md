# server

### Compiles and emulates firebase
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Run your unit tests
```
yarn test:unit
```

### Connect to the Firebase shell
```
yarn shell
```

### View the Firebase logs
```
yarn logs
```
