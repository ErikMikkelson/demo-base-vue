module.exports = {
    moduleFileExtensions: ["js", "ts"],
    transform: {
        "^.+\\.ts$": "ts-jest"
    },
    transformIgnorePatterns: ["node_modules"],
    moduleNameMapper: {
        "^@/(.*)$": "<rootDir>/src/$1"
    },
    testMatch: ["<rootDir>/src/**/(*.)spec.(ts)"]
};
