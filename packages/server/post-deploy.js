/* eslint-disable @typescript-eslint/no-var-requires */
const packageJsonPath = "./package.json";
const packageJson = require(packageJsonPath);
const fs = require("fs-extra");

(async () => {
    // This is a hack because firebase-tools doesn't support monorepos
    // Will need to be done with ALL shared dependencies
    await fs.remove(`./shared`);

    packageJson.dependencies["shared"] = "1.0.0";

    await fs.writeFile(packageJsonPath, JSON.stringify(packageJson, null, 2));
})();
