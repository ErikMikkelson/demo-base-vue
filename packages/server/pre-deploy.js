/* eslint-disable @typescript-eslint/no-var-requires */
const fs = require("fs-extra");

const packageJsonPath = "./package.json";
const packageJson = require(packageJsonPath);

(async () => {
    // This is a hack because firebase-tools doesn't support monorepos
    // Will need to be done with ALL shared dependencies
    await fs.remove(`./shared`);
    await fs.copy(`../shared`, `./shared`);

    packageJson.dependencies["shared"] = "file:./shared";

    await fs.writeFile(packageJsonPath, JSON.stringify(packageJson, null, 2));
})();
