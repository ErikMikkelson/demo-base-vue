import * as functions from "firebase-functions";
import { ShapeType } from "shared/enums/Shape";
import { BadRequest, Internal } from "../services/FirebaseErrors";
import { getOutcomeType } from "../services/RulesEngine";

// Not using StackDriver logging here to prevent charging

function randomEnum<T>(anEnum: T): T[keyof T] {
    const enumValues = (Object.values(anEnum) as unknown) as T[keyof T][];
    const randomIndex = Math.floor(Math.random() * enumValues.length);

    return enumValues[randomIndex];
}

export default functions.https.onCall(async (_data, _context) => {
    const maybeClientShape: ShapeType | undefined = _data as ShapeType;

    if (maybeClientShape !== undefined) {
        try {
            const serverShape = randomEnum(ShapeType);
            const outcome = getOutcomeType(maybeClientShape, serverShape);

            const result = {
                client: maybeClientShape,
                server: serverShape,
                outcome
            };

            return result;
        } catch (err) {
            const errData = {
                err,
                data: _data,
                context: _context
            };

            console.error(`Error handling request: ${JSON.stringify(errData, null, 2)}`);
            throw new Internal();
        }
    } else {
        throw new BadRequest();
    }
});
