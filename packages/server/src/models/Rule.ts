import { OutcomeType } from "shared/enums/Outcome";
import { ShapeType } from "shared/enums/Shape";

export class Rule {
    outcome: OutcomeType;
    client: ShapeType;
    server: ShapeType;
}