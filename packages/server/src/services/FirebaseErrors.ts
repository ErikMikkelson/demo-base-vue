import * as functions from "firebase-functions";

export class BadRequest extends functions.https.HttpsError {
    constructor(message = "Bad Request") {
        super("invalid-argument", message);
    }
}

export class NotFound extends functions.https.HttpsError {
    constructor() {
        super("not-found", "Not Found");
    }
}

export class Permission extends functions.https.HttpsError {
    constructor() {
        super("permission-denied", "Permission Denied");
    }
}

export class Unauthenticated extends functions.https.HttpsError {
    constructor() {
        super("unauthenticated", "Unauthenticated");
    }
}

export class Internal extends functions.https.HttpsError {
    constructor() {
        super("internal", "Server Error");
    }
}
