import * as RulesEngine from "./RulesEngine";
import { ShapeType } from "shared/enums/Shape";
import { OutcomeType } from "shared/enums/Outcome";

describe("RulesEngine", () => {
    describe("getOutreachType", () => {
        it("should return return outcome TIE when both shapes are ROCK", () => {
            const result = RulesEngine.getOutcomeType(ShapeType.ROCK, ShapeType.ROCK);

            expect(result).toEqual(OutcomeType.TIE);
        });

        it("should return return outcome TIE when both shapes are PAPER", () => {
            const result = RulesEngine.getOutcomeType(ShapeType.PAPER, ShapeType.PAPER);

            expect(result).toEqual(OutcomeType.TIE);
        });

        it("should return return outcome TIE when both shapes are SCISSORS", () => {
            const result = RulesEngine.getOutcomeType(ShapeType.SCISSORS, ShapeType.SCISSORS);

            expect(result).toEqual(OutcomeType.TIE);
        });

        it("should return return outcome WIN when ROCK beats SCISSORS", () => {
            const result = RulesEngine.getOutcomeType(ShapeType.ROCK, ShapeType.SCISSORS);

            expect(result).toEqual(OutcomeType.WIN);
        });

        it("should return return outcome WIN when PAPER beats ROCK", () => {
            const result = RulesEngine.getOutcomeType(ShapeType.PAPER, ShapeType.ROCK);

            expect(result).toEqual(OutcomeType.WIN);
        });

        it("should return return outcome WIN when SCISSORS beats PAPER", () => {
            const result = RulesEngine.getOutcomeType(ShapeType.SCISSORS, ShapeType.PAPER);

            expect(result).toEqual(OutcomeType.WIN);
        });

        it("should return return outcome LOSE when ROCK loses to PAPER", () => {
            const result = RulesEngine.getOutcomeType(ShapeType.ROCK, ShapeType.PAPER);

            expect(result).toEqual(OutcomeType.LOSE);
        });

        it("should return return outcome LOSE when PAPER loses to SCISSORS", () => {
            const result = RulesEngine.getOutcomeType(ShapeType.PAPER, ShapeType.SCISSORS);

            expect(result).toEqual(OutcomeType.LOSE);
        });

        it("should return return outcome WIN when SCISSORS loses to ROCK", () => {
            const result = RulesEngine.getOutcomeType(ShapeType.SCISSORS, ShapeType.ROCK);

            expect(result).toEqual(OutcomeType.LOSE);
        });
    });
});
