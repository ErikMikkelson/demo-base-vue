import { Rule } from "../models/Rule";
import { ShapeType } from "shared/enums/Shape";
import { OutcomeType } from "shared/enums/Outcome";

const rules: Rule[] = [
    {
        client: ShapeType.ROCK,
        server: ShapeType.ROCK,
        outcome: OutcomeType.TIE
    },
    {
        client: ShapeType.PAPER,
        server: ShapeType.PAPER,
        outcome: OutcomeType.TIE
    },
    {
        client: ShapeType.SCISSORS,
        server: ShapeType.SCISSORS,
        outcome: OutcomeType.TIE
    },
    {
        client: ShapeType.ROCK,
        server: ShapeType.SCISSORS,
        outcome: OutcomeType.WIN
    },
    {
        client: ShapeType.PAPER,
        server: ShapeType.ROCK,
        outcome: OutcomeType.WIN
    },
    {
        client: ShapeType.SCISSORS,
        server: ShapeType.PAPER,
        outcome: OutcomeType.WIN
    },
    {
        client: ShapeType.SCISSORS,
        server: ShapeType.ROCK,
        outcome: OutcomeType.LOSE
    },
    {
        client: ShapeType.ROCK,
        server: ShapeType.PAPER,
        outcome: OutcomeType.LOSE
    },
    {
        client: ShapeType.PAPER,
        server: ShapeType.SCISSORS,
        outcome: OutcomeType.LOSE
    }
];

export function getOutcomeType(client: ShapeType, server: ShapeType): OutcomeType {
    let outcomeType = undefined;

    rules.forEach(rule => {
        if (client === rule.client && server === rule.server) {
            outcomeType = rule.outcome;

            return;
        }
    });

    if (!outcomeType) {
        throw new Error(`No outcome defined for ${client} vs ${server}!`);
    }

    return outcomeType;
}
