export enum OutcomeType {
    WIN = "Win",
    LOSE = "Lose",
    TIE = "Tie"
}
