export enum ShapeType {
    ROCK = "Rock",
    PAPER = "Paper",
    SCISSORS = "Scissors"
}
