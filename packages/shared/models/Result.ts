import { OutcomeType } from "../enums/Outcome";
import { ShapeType } from "../enums/Shape";

export class Result {
    // Keeping this simple
    client: ShapeType;
    server: ShapeType;
    outcome: OutcomeType;
}
